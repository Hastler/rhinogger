# Rhinogger #

The simple remote logger for mobile (cordova) apps written on Node.js and Angular.js.

### How I can send a logs to Rhinogger? ###

Very simple. Just send a post request to server.

* JQuery:
$.post(url, {message: logMessage});

* Angular
$http.post(url, {message: logMessage});

note: by default, server will run on 3000 port, add this to url string.

### How do I get set up? ###

1 Clone repository:
clone git@bitbucket.org:Hastler/rhinogger.git
cd rhinogger

2 Install dependencies:
npm install

3 Run server
npm start