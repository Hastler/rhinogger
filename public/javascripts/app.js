'use strict';

var rhinoApp = angular.module('rhinoApp', ['luegg.directives']);

rhinoApp.controller('logsController', ['$scope', '$interval', '$http', '$anchorScroll',
  function($scope) {
    var
      socket = io();

    $scope.devices = [];
    $scope.modules = [{
      name: 'unknown',
      color: 'module_unknown'
    }];

    $scope.selectedDevice = 0;
    $scope.search = "";

    $scope.selectDevice = function(device) {
      $scope.selectedDevice = device ? device.uuid : 0;
    };

    $scope.clearLog = function(device) {
      device.messages = [];
    };

    $scope.closeDevice = function(device) {
      $scope.devices = $scope.devices.filter(function(d) {
        return device.uuid !== d.uuid;
      });

      if (device.uuid === $scope.selectedDevice) {
        $scope.selectDevice($scope.devices[0]);
      }
    };

    socket.on('log:message', function(message) {
      $scope.$apply(function() {
        var

          device = $scope.devices.reduce(function(previous, current) {
            return previous || (current.uuid === message.device.uuid ? current : false);
          }, false),

          message_module = message.module || 'unknown',

          module = $scope.modules.reduce(function(previous, current) {
            return previous || (current.name === message_module ? current : false);
          }, false),

          color = 'module_' + $scope.modules.length;

        if (!module) {
          $scope.modules.push({
            name: message.module,
            color: color
          });
        } else {
          color = module.color;
        }

        if (device) {
          device.messages.push({
            text: message.text,
            color: color
          });
        } else {

          if (!$scope.devices.length) {
            $scope.selectedDevice = message.device.uuid;
          }

          $scope.devices.push({
            uuid: message.device.uuid,
            caption: message.device.platform,
            messages: [{
              text: message.text,
              color: color
            }]
          });
        }
      });
    });

}]);

